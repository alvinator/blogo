package main

import (
	"os"
	"fmt"
	"flag"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"html/template"
	"encoding/json"
	"launchpad.net/mgo"
	"launchpad.net/mgo/bson"
)

type Blog struct {
	Title string
	Slug string
	Body string
}

type Url struct {
	Url string
	Handler string
}

const lenPath = len("/blog/")

func blogHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[lenPath:]
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	c := session.DB("blogo").C("blogs")	
	result := Blog{}

	if title == "" {
		results := []map[string]string{}
		iter := c.Find(nil).Iter()
		for iter.Next(&result) {
			results = append(results, map[string]string {"Slug": result.Slug, "Title": result.Title, "Body": result.Body})
		}
		t, _ := template.ParseFiles("templates/list.html")
		t.Execute(w, results)
	} else {
		err := c.Find(bson.M{"slug": title}).One(&result)
		if err != nil {
			panic(err)
		}
		t, err := template.ParseFiles("templates/view.html")
		t.Execute(w, result)
		 
	}
	return
}

func editHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("templates/edit.html")
	if r.Method != "POST" {
		t.Execute(w, nil)
	} else {
		title := r.FormValue("title")
		slug := r.FormValue("slug")
		body := r.FormValue("body")
		session, err := mgo.Dial("127.0.0.1")
		if err != nil {
			panic(err)
		}
		c := session.DB("blogo").C("blogs")
		err = c.Insert(&Blog{title, slug, body})
		if err != nil {
			panic(err)
		}
	} 	
}

func main() {
	// process flags
	var listen *string = flag.String("listen", "127.0.0.1:9000", "IP:Port to bind to")
	var url_conf *string = flag.String("urls", "conf.json", "url config file")
	fmt.Printf("\n-=blogo running on %s using %s urls=-\n\n", *listen, *url_conf);
	flag.Parse()
	// parse urls
	root, _ := filepath.Split(filepath.Clean(os.Args[0]))
	b, err := ioutil.ReadFile(filepath.Join(root, *url_conf))
	var urls []Url
	json.Unmarshal(b, &urls);

	http.HandleFunc("/blog/", blogHandler)
	http.HandleFunc("/blog/edit/", editHandler)
	http_err := http.ListenAndServe(*listen, nil)
	if http_err != nil {
		panic(err)
	}
}